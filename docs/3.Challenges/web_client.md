---
hide:
  - toc
author: à compléter
title: Liste des challenges
---

# Web - client


!!! abstract "Description générale de la catégorie"

    Le Web représente l'ensemble des liaisons entre les différents équipements électroniques et informatiques qui communiquent. Le versant &laquo;client&raquo; correspond à l'application qu'une personne utilisera pour naviguer sur le web, par exemple un navigateur web.

    Dans cette catégorie, on propose divers challenges permettant de manipuler quelques fonctionnalités d'un navigateur web, du plus simple comme l'affichage d'un code source, au plus compliqué avec l'utilisation de son inspecteur de code, en passant par la lecture de programme javascript.


!!! warning "Autres challenges"

    D'autres challenges dans différentes catégories sont également disponibles sur le <a href='https://cybersecurite.forge.apps.education.fr/cyber/3.Challenges/presentation/' target='_blank'>site principal</a> dédié à la cybersécurité.

    Etes-vous prêt à relever les défis ?



<hr style="height:5px;color:red;background-color:red;">

!!! note "Capture The Fraise I"

    !!! tip "Présentation"

        **Niveau :** collège, SNT, NSI

        **Difficulté :** ⭐ 1 flag

        **Objectif :** comprendre la structure d'un site web

    ![](../ressources_challenges_web_client/CaptureTheFraise_N1/fraise_N1.png){: .center }
    
    Explorez un site web pour reconstituer le flag perdu

    N'oubliez pas : un site web n'est pas qu'une simple page html...

    [_Accéder au challenge_](../ressources_challenges_web_client/CaptureTheFraise_N1/capture_the_fraise_N1){ .md-button target="_blank" rel="noopener" }


!!! note "Capture The Fraise II"

    !!! tip "Présentation"

        **Niveau :** SNT, NSI

        **Difficulté :** ⭐⭐ 1 flag

        **Objectif :** manipuler la console javascript de l'inspecteur du navigateur

    ![](../ressources_challenges_web_client/CaptureTheFraise_N2/fraise_N2.png){: .center }
    
    Devenez le champion de la récolte de fraises et explosez le score en un minimum de temps pour obtenir le flag !

    [_Accéder au challenge_](../ressources_challenges_web_client/CaptureTheFraise_N2/capture_the_fraise_N2){ .md-button target="_blank" rel="noopener" }


!!! note "Capture The Fraise III"

    !!! tip "Présentation"

        **Niveau :** NSI

        **Difficulté :** ⭐⭐⭐ 1 flag

        **Objectif :** manipuler la console javascript de l'inspecteur du navigateur

    ![](../ressources_challenges_web_client/CaptureTheFraise_N3/fraise_N3.png){: .center }
    
    Bravo pour votre récolte de fraises ! Mais le flag ne vous sera dévoilé que si vous réussissez à effectuer la 
    bonne sauvegarde...

    [_Accéder au challenge_](../ressources_challenges_web_client/CaptureTheFraise_N3/capture_the_fraise_N3){ .md-button target="_blank" rel="noopener" }


!!! note "Capture The Fraise IV"

    !!! tip "Présentation"

        **Niveau :** NSI

        **Difficulté :** ⭐⭐⭐ 1 flag

        **Objectif :** mmanipuler des fichiers textes pour modifier le comportement d'un site Web

    ![](../ressources_challenges_web_client/CaptureTheFraise_N4/fraise_N4.png){: .center }
    
    Une dernière partie sur votre PC x86 avant la fin du monde informatique ?

    Si vous y arrivez, le flag vous sera dévoilé....

    [_Accéder au challenge_](../ressources_challenges_web_client/CaptureTheFraise_N4/capture_the_fraise_N4){ .md-button target="_blank" rel="noopener" }


!!! note "Connexion administrateur"

    !!! tip "Présentation"

        **Niveau :** NSI

        **Difficulté :** ⭐⭐ 5 flags

        **Objectif :** aborder la problématique de la sécurisation des mots de passe

    ![](../ressources_challenges_web_client/ConnexionAdministrateur/image_connexion.png){: .center }
    
    Vous venez d'accéder à l'interface de connexion d'un administrateur système. Plus qu'un pas et vous obtiendrez son flag !
    
    Une initiation aux bonnes pratiques sur le développement Web.

    [_Accéder au challenge_](../ressources_challenges_web_client/ConnexionAdministrateur/connexion_administrateur){ .md-button target="_blank" rel="noopener" }


!!! note "Cookies or not cookies (challenge issu de la plateforme <a href='https://www.challenges-kids.fr/index.php' target='_blank'>challenges-kids.fr</a>)"

    !!! tip "Présentation"

        **Niveau :** collège, SNT, NSI

        **Difficulté :** ⭐ 1 flag

        **Objectif :** comprendre la notion de cookie

    ![](../ressources_challenges_web_client/Challenges_externes/cookies.png){: .center }
    
    Des cookies, des cookies et encore des cookies...
    
    Arriverez-vous à trouver le flag qui s'est caché parmi les options sur les cookies ?

    <a href='https://www.challenges-kids.fr/index.php' target='_blank'>Challenges-kids.fr</a> est une plateforme ouverte à la contribution et d'aide à l'enseignement du Hacking pour les petits et les grands, qui offre de nombreux challenges de difficultés graduées pour progresser.

    [_Accéder au challenge_](https://www.challenges-kids.fr/categories/culture/chall3/){ .md-button target="_blank" rel="noopener" }


!!! note "HTML - boutons désactivés (challenge issu de la plateforme <a href='https://www.root-me.org/' target='_blank'>Root Me</a>)"

    !!! tip "Présentation"

        **Niveau :** collège, SNT, NSI

        **Difficulté :** ⭐ 1 flag

        **Objectif :** manipuler l'inspecteur du navigateur

    ![](../ressources_challenges_web_client/Challenges_externes/HTML_boutons_desactives.png){: .center }
    
    Un formulaire inutilisable ? A vous de contourner la situation pour récupérer le flag !

    <a href='https://www.root-me.org/' target='_blank'>Root Me</a> est une plateforme de CTF française très connue et très qualitative qui offre de nombreux challenges dans diverses catégories, ainsi que de nombreuses ressources.

    [_Accéder au challenge_](https://www.root-me.org/fr/Challenges/Web-Client/HTML-boutons-desactives){ .md-button target="_blank" rel="noopener" }


!!! note "Motifs"

    !!! tip "Présentation"

        **Niveau :** SNT, NSI

        **Difficulté :** ⭐ 5 flags

        **Objectif :** comprendre le mécanisme de vérification automatique des champs dans un formulaire

    ![](../ressources_challenges_web_client/Motifs/image_chall_motifs.png){: .center }
    
    Cinq connexions à réaliser... Cinq énigmes... Cinq flags...

    Y parviendrez-vous ?

    [_Accéder au challenge_](../ressources_challenges_web_client/Motifs/motifs){ .md-button target="_blank" rel="noopener" }