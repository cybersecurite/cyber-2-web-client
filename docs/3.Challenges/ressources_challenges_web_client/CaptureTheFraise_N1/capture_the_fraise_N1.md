---
hide:
  - toc
author: à compléter
title: Capture The Fraise I
---

# Challenge : Capture The Fraise I

![](../fraise_N1.png){: .center }

!!! note "Votre objectif"
    Ce <a href='https://cybersecurite.forge.apps.education.fr/fraise/f/' target='_blank'>site</a> contient un flag en trois parties. Vous devez reconstituer ce flag par concaténation.

    Chacune des parties est écrite au format `N1F1{flag1}`, `N1F2{flag2}` et `N1F3{flag3}`

    Par exemple, si on a trouvé `N1F1{carotte}`, `N1F2{banane}` et `N1F3{chocolat}`, alors le flag est `carottebananechocolat`

??? abstract "Indice"
    Quels sont les trois langages du Web ?


<hr style="height:5px;color:red;background-color:red;">
<center>A vous de jouer !</center>
<hr style="height:5px;color:red;background-color:red;">

<div id='Partie1'>
<p>
    <form id="form_capture_the_fraise_n1">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte1'></p>
</div>



<div id='Final' markdown="1" hidden>
<br>
<h2>Félicitations !</h2>
<p>Les trois langages du Web n'ont pas de secret pour vous !</p>

!!! danger "A retenir"

    Un site web est composé de plusieurs types de fichiers.
    
    Parmi les plus courants, on trouve les fichiers html pour le contenu, les fichiers CSS pour le style et les fichiers js pour le rendu interactif, ainsi que des fichiers annexes tels que des images, des vidéos, des fichiers pdf, des fichiers txt, etc.

    Pour des sites plus élaborés, on peut également trouver des fichiers php qui permettent de rendre le site dynamique et de le connecté, par exemple, à des bases de données. 

</div>

<script src='../script_chall_capture_the_fraise_N1.js'></script>