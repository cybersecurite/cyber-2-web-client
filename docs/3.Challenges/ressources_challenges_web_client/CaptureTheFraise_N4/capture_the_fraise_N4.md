---
hide:
  - toc
author: à compléter
title: Capture The Fraise IV
---

# Challenge : Capture The Fraise IV

![](../fraise_N4.png){: .center }

!!! note "Votre objectif"
    Vous venez de réaliser la sauvegarde de votre dernière partie sur ce 
    <a href='https://cybersecurite.forge.apps.education.fr/fraise/f/' target='_blank'>site</a> lors du `Capture The Fraise III` !
    
    Maintenant, vous souhaitez continuer cette partie une ultime fois sur votre PC x86 avant la fin du monde informatique !

    Dépêchez-vous d'aller sur ce  avant qu'il ne soit trop tard !

    
??? abstract "Indice"
    tic tac tic tac ... : overflow !


<hr style="height:5px;color:red;background-color:red;">
<center>A vous de jouer !</center>
<hr style="height:5px;color:red;background-color:red;">

<div id='Partie1'>
<p>
    <form id="form_capture_the_fraise_n4">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte1'></p>
</div>



<div id='Final' markdown="1" hidden>
<br>
<h2>Félicitations !</h2>
<p>Vous maîtrisez les timestamps !</p>

!!! danger "A retenir"

    L’importation de fichiers externes (qu’ils soient en JavaScript, texte ou autres formats) peut modifier le comportement d’un site web et introduire des risques en cybersécurité. Il est essentiel de s'assurer que ces fichiers proviennent de sources fiables, de vérifier régulièrement leur contenu pour détecter d’éventuels changements, et de valider toutes les données avant de les utiliser.

</div>

<script src='../script_chall_capture_the_fraise_N4.js'></script>