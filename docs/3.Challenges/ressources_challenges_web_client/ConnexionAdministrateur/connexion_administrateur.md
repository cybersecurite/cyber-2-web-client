---
hide:
  - toc
author: à compléter
title: Connexion administrateur
---

# Challenge : connexion administrateur

![](../image_connexion.png){: .center }

!!! note "Votre objectif"
    Vous avez réussi à atteindre la page de connexion de l'administrateur d'un système réseau.
    
    Pour chacune des situations ci-dessous, vous devez réussir à vous authentifier à sa place afin de récupérer son flag.

<hr style="height:5px;color:red;background-color:red;">
<center>A vous de jouer !</center>
<hr style="height:5px;color:red;background-color:red;">

<div id='Partie1'>
<h2>Situation 1 : mot de passe faible</h2>
<p>Une capture d'écran de la page de connexion est donnée <a href='../Connexion1.png' target='_blank'>ici</a>.
<br><br>
Le flag est le mot de passe utilisé par l'administrateur pour se connecter à son compte personnel.</p>
<br>
<p>
    <form id="form_connexion_administrateur1">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte1'></p>
</div>


<div id='Partie2' hidden>
<br>
<h2>Situation 2 : en clair</h2>
<p>Connectez vous en tant qu'administrateur sur cette <a href='../connexion2.html' target='_blank'>page</a> pour obtenir le flag.</p>
<br>
<p>
    <form id="form_connexion_administrateur2">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte2'></p>
</div>

<div id='Partie3' hidden>
<br>
<h2>Situation 3 : obfuscation</h2>
<p>Connectez vous en tant qu'administrateur sur cette <a href='../connexion3.html' target='_blank'>page</a> pour obtenir le flag.</p>
<br>
<p>
    <form id="form_connexion_administrateur3">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte3'></p>
</div>

<div id='Partie4' hidden>
<br>
<h2>Situation 4 : bizarre...</h2>
<p>Connectez vous en tant qu'administrateur sur cette <a href='../connexion4.html' target='_blank'>page</a> pour obtenir le flag.</p>
<br>
<p>
    <form id="form_connexion_administrateur4">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte4'></p>
</div>

<div id='Partie5' hidden>
<br>
<h2>Situation 5 : hachage</h2>
<p>Utilisez les informations contenues dans cette <a href='../Connexion5.png' target='_blank'>copie d'écran</a> de la page de connexion de l'administrateur pour déterminer son flag.</p>
<br>
<p>
    <form id="form_connexion_administrateur5">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte5'></p>
</div>

<div id='Final' markdown="1" hidden>
<br>
<h2>Félicitations !</h2>
<p>Vous avez récupéré tous les flags !</p>

!!! danger "A retenir"

    Attention à la gestion de vos mots de passe :

     - choisissez des mots de passe robustes ;
     - ne choisissez pas des mots de passe trop proches pour la gestion d'applications sensibles ;
     - ne laissez pas traîner des informations sensibles sans les protéger avec des mots de passe efficaces ;
     - ne faites jamais de vérification de mot de passe côté client, même s'ils ont été hachés !

    Pour plus d'informations, n'hésitez pas à consulter cette <a href='https://www.cybermalveillance.gouv.fr/tous-nos-contenus/bonnes-pratiques/mots-de-passe' target='_blank'>page</a>.

</div>

<script src='../script_chall_connexion_administrateur.js'></script>