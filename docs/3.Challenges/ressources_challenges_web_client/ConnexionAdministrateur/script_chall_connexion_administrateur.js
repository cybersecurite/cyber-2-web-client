const flags = {'connexion_administrateur1':['admin',1,1],  // [flag, numéro de la partie dans le challenge, partie suivante ou pas]
				'connexion_administrateur2':['4dm1nP@sSw0rD',2,1],
				'connexion_administrateur3':['@dM1n1sTrat1oNm0t_de_P@sS3',3,1],
				'connexion_administrateur4':['AdM1n_R3s3@u{0rD1n@t3uR_pr1nC1P@L}',4,1],
				'connexion_administrateur5':['admin@1Adm1n',5,0]};
 

function check_and_update(event)
{
    event.preventDefault();

    const f = event.target;
    const f_name = f.id.substring(5); // Extraction de la clé du dictionnaire dans le nom du formulaire
    const texte = document.getElementById('Texte'+flags[f_name][1].toString());
    if (f.flag.value===flags[f_name][0]) // Réussite à un challenge
	{
		texte.innerHTML = 'Bravo ! Flag validé !';
        texte.style.color = 'black';
        if(flags[f_name][2]===0) // Réussite du dernier challenge
		{
			document.getElementById('Final').hidden = false;
		}
		else
		{
			const val_part = flags[f_name][1]+flags[f_name][2];
			document.getElementById('Partie'+val_part.toString()).hidden = false;
		}
	}
	else 
	{
		texte.innerHTML = 'Flag incorrect ! Réessayez !';
        texte.style.color = 'red';
	}
}

// formulaires commencent par form_. formulaire de la forme : form_ + clé du dictionnaire flags
form_connexion_administrateur1.addEventListener( "submit", check_and_update);
form_connexion_administrateur2.addEventListener( "submit", check_and_update);
form_connexion_administrateur3.addEventListener( "submit", check_and_update);
form_connexion_administrateur4.addEventListener( "submit", check_and_update);
form_connexion_administrateur5.addEventListener( "submit", check_and_update);