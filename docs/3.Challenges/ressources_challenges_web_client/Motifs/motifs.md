---
hide:
  - toc
author: à compléter
title: Motifs
---

# Challenge : motifs

![](../image_chall_motifs.png){: .center }

!!! note "Votre objectif"
    Réussissez à passer les différentes épreuves de connexion pour obtenir les flags

<hr style="height:5px;color:red;background-color:red;">
<center>A vous de jouer !</center>
<hr style="height:5px;color:red;background-color:red;">

<div id='Partie1'>
<h2>Enigme 1</h2>
<p>Connectez vous sur cette <a href='../chall_motif_enigme1.html' target='_blank'>page</a> pour obtenir le flag.
</p>
<br>
<p>
    <form id="form_motif1">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte1'></p>
</div>


<div id='Partie2' hidden>
<br>
<h2>Enigme 2</h2>
<p>Connectez vous sur cette <a href='../chall_motif_enigme2.html' target='_blank'>page</a> pour obtenir le flag.
</p>
<br>
<p>
    <form id="form_motif2">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte2'></p>
</div>

<div id='Partie3' hidden>
<br>
<h2>Enigme 3</h2>
<p>Connectez vous sur cette <a href='../chall_motif_enigme3.html' target='_blank'>page</a> pour obtenir le flag.
</p>
<br>
<p>
    <form id="form_motif3">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte3'></p>
</div>

<div id='Partie4' hidden>
<br>
<h2>Enigme 4</h2>
<p>Connectez vous sur cette <a href='../chall_motif_enigme4.html' target='_blank'>page</a> pour obtenir le flag.
</p>
<br>
<p>
    <form id="form_motif4">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte4'></p>
</div>

<div id='Partie5' hidden>
<br>
<h2>Enigme 5</h2>
<p>Connectez vous sur cette <a href='../chall_motif_enigme5.html' target='_blank'>page</a> pour obtenir le flag.
</p>
<br>
<p>
    <form id="form_motif5">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte5'></p>
</div>

<div id='Final' markdown="1" hidden>
<br>
<h2>Félicitations !</h2>
<p>Vous avez récupéré tous les flags ! Les expressions régulières n'ont pas de secrets pour vous !</p>

!!! danger "A retenir"

    La vérification automatique des champs d'un formulaire côté client se fait le plus simple avec des expressions régulières qui sont des outils très performants pour trouver des motifs à l'intérieur d'un texte.

    Pour plus de détails, on pourra consulter cette <a href='https://cybersecurite.forge.apps.education.fr/cyber/2.Boite_outils/expressions_regulieres/' target='_blank'>page</a>.

</div>

<script src='../script_chall_motifs.js'></script>